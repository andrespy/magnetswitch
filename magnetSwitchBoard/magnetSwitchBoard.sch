EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L magnetSwitch:MH248 U1
U 1 1 5ED0AAC1
P 6100 2600
F 0 "U1" H 6100 2825 50  0000 C CNN
F 1 "MH248" H 6100 2734 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-92S_Wide" H 6050 3300 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/szlcsc/1811061517_MST-Magnesensor-Tech-MST-MH248EUA_C114366.pdf" H 6050 3300 50  0001 C CNN
F 4 "C114366" H 6100 2600 50  0001 C CNN "LCSC PART #"
	1    6100 2600
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0101
U 1 1 5ED0B3A1
P 6100 2950
F 0 "#PWR0101" H 6100 2700 50  0001 C CNN
F 1 "GND" H 6105 2777 50  0000 C CNN
F 2 "" H 6100 2950 50  0001 C CNN
F 3 "" H 6100 2950 50  0001 C CNN
	1    6100 2950
	1    0    0    -1  
$EndComp
$Comp
L magnetSwitch:LD1117V33 U3
U 1 1 5ECAEAD6
P 4150 2600
F 0 "U3" H 4150 2887 60  0000 C CNN
F 1 "LD1117V33" H 4150 2781 60  0000 C CNN
F 2 "Package_TO_SOT_SMD:TO-252-2" H 4350 2800 60  0001 L CNN
F 3 "https://datasheet.lcsc.com/szlcsc/1912111437_STMicroelectronics-LD1117DT33TR_C132629.pdf" H 4350 2900 60  0001 L CNN
F 4 "LD1117DT33TR" H 4350 3100 60  0001 L CNN "MPN"
F 5 "Integrated Circuits (ICs)" H 4350 3200 60  0001 L CNN "Category"
F 6 "PMIC - Voltage Regulators - Linear" H 4350 3300 60  0001 L CNN "Family"
F 7 "C132629" H 4350 2350 50  0001 C CNN "LCSC PART #"
F 8 "IC REG LINEAR 3.3V 800MA TO220AB" H 4350 3600 60  0001 L CNN "Description"
F 9 "STMicroelectronics" H 4350 3700 60  0001 L CNN "Manufacturer"
F 10 "Active" H 4350 3800 60  0001 L CNN "Status"
	1    4150 2600
	1    0    0    -1  
$EndComp
$Comp
L Device:C C1
U 1 1 5ECAEFCF
P 3500 2750
F 0 "C1" H 3615 2796 50  0000 L CNN
F 1 "100nF" H 3615 2705 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 3538 2600 50  0001 C CNN
F 3 "~" H 3500 2750 50  0001 C CNN
F 4 "C14663" H 3500 2750 50  0001 C CNN "LCSC PART #"
	1    3500 2750
	1    0    0    -1  
$EndComp
$Comp
L Device:C C2
U 1 1 5ECAF27C
P 4850 2750
F 0 "C2" H 4965 2796 50  0000 L CNN
F 1 "10uF" H 4965 2705 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 4888 2600 50  0001 C CNN
F 3 "~" H 4850 2750 50  0001 C CNN
F 4 "C19702" H 4850 2750 50  0001 C CNN "LCSC PART #"
	1    4850 2750
	1    0    0    -1  
$EndComp
Wire Wire Line
	3500 2600 3850 2600
Wire Wire Line
	3500 2900 4150 2900
Wire Wire Line
	4150 2900 4850 2900
Connection ~ 4150 2900
Wire Wire Line
	4850 2600 4800 2600
$Comp
L power:GND #PWR06
U 1 1 5ECB0022
P 4150 2900
F 0 "#PWR06" H 4150 2650 50  0001 C CNN
F 1 "GND" H 4155 2727 50  0000 C CNN
F 2 "" H 4150 2900 50  0001 C CNN
F 3 "" H 4150 2900 50  0001 C CNN
	1    4150 2900
	1    0    0    -1  
$EndComp
$Comp
L Device:Fuse F1
U 1 1 5ECB1781
P 2300 2600
F 0 "F1" V 2103 2600 50  0000 C CNN
F 1 "Fuse" V 2194 2600 50  0000 C CNN
F 2 "" V 2230 2600 50  0001 C CNN
F 3 "~" H 2300 2600 50  0001 C CNN
	1    2300 2600
	0    1    1    0   
$EndComp
$Comp
L Connector:Screw_Terminal_01x02 J1
U 1 1 5ECB1C2C
P 1650 2700
F 0 "J1" H 1568 2375 50  0000 C CNN
F 1 "Screw_Terminal_01x02" H 1568 2466 50  0000 C CNN
F 2 "TerminalBlock:TerminalBlock_bornier-2_P5.08mm" H 1650 2700 50  0001 C CNN
F 3 "~" H 1650 2700 50  0001 C CNN
	1    1650 2700
	-1   0    0    1   
$EndComp
Wire Wire Line
	1850 2700 1850 2900
Wire Wire Line
	2150 2600 1850 2600
$Comp
L power:+12V #PWR04
U 1 1 5ECB2C8C
P 3100 2600
F 0 "#PWR04" H 3100 2450 50  0001 C CNN
F 1 "+12V" H 3115 2773 50  0000 C CNN
F 2 "" H 3100 2600 50  0001 C CNN
F 3 "" H 3100 2600 50  0001 C CNN
	1    3100 2600
	1    0    0    -1  
$EndComp
Connection ~ 3500 2600
Connection ~ 3500 2900
Connection ~ 3100 2600
Wire Wire Line
	3100 2600 3500 2600
Wire Wire Line
	1850 2900 2750 2900
Wire Wire Line
	2450 2600 2750 2600
$Comp
L Device:D D1
U 1 1 5ECB64A7
P 2750 2750
F 0 "D1" V 2704 2829 50  0000 L CNN
F 1 "D" V 2795 2829 50  0000 L CNN
F 2 "Diode_SMD:D_SMA_Handsoldering" H 2750 2750 50  0001 C CNN
F 3 "~" H 2750 2750 50  0001 C CNN
F 4 " C95872" H 2750 2750 50  0001 C CNN "LCSC PART #"
	1    2750 2750
	0    1    1    0   
$EndComp
Connection ~ 2750 2600
Wire Wire Line
	2750 2600 3100 2600
Connection ~ 2750 2900
Wire Wire Line
	2750 2900 3500 2900
$Comp
L power:+3V3 #PWR05
U 1 1 5ECB696C
P 4800 2600
F 0 "#PWR05" H 4800 2450 50  0001 C CNN
F 1 "+3V3" H 4815 2773 50  0000 C CNN
F 2 "" H 4800 2600 50  0001 C CNN
F 3 "" H 4800 2600 50  0001 C CNN
	1    4800 2600
	1    0    0    -1  
$EndComp
Connection ~ 4800 2600
Wire Wire Line
	4800 2600 4450 2600
$Comp
L Device:C C3
U 1 1 5ECC4F60
P 5550 2750
F 0 "C3" H 5665 2796 50  0000 L CNN
F 1 "10nF" H 5665 2705 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 5588 2600 50  0001 C CNN
F 3 "~" H 5550 2750 50  0001 C CNN
F 4 "C57112" H 5550 2750 50  0001 C CNN "LCSC PART #"
	1    5550 2750
	1    0    0    -1  
$EndComp
Wire Wire Line
	5550 2600 5650 2600
Wire Wire Line
	5550 2900 5550 2950
Wire Wire Line
	5550 2950 6100 2950
Connection ~ 6100 2950
Wire Wire Line
	5550 2600 4850 2600
Connection ~ 5550 2600
Connection ~ 4850 2600
$Comp
L power:+12V #PWR08
U 1 1 5ECD6242
P 7800 3150
F 0 "#PWR08" H 7800 3000 50  0001 C CNN
F 1 "+12V" H 7815 3323 50  0000 C CNN
F 2 "" H 7800 3150 50  0001 C CNN
F 3 "" H 7800 3150 50  0001 C CNN
	1    7800 3150
	1    0    0    -1  
$EndComp
$Comp
L Amplifier_Operational:LM358 U2
U 1 1 5ECD86DD
P 7800 2500
F 0 "U2" H 7800 2867 50  0000 C CNN
F 1 "LM358" H 7800 2776 50  0000 C CNN
F 2 "Package_SO:SOP-8_3.9x4.9mm_P1.27mm" H 7800 2500 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm2904-n.pdf" H 7800 2500 50  0001 C CNN
	1    7800 2500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR09
U 1 1 5ECD6940
P 7800 3750
F 0 "#PWR09" H 7800 3500 50  0001 C CNN
F 1 "GND" H 7805 3577 50  0000 C CNN
F 2 "" H 7800 3750 50  0001 C CNN
F 3 "" H 7800 3750 50  0001 C CNN
	1    7800 3750
	1    0    0    -1  
$EndComp
$Comp
L Amplifier_Operational:LM358 U2
U 3 1 5ECBB33D
P 7900 3450
F 0 "U2" H 7858 3496 50  0000 L CNN
F 1 "LM358" H 7858 3405 50  0000 L CNN
F 2 "Package_SO:SOP-8_3.9x4.9mm_P1.27mm" H 7900 3450 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm2904-n.pdf" H 7900 3450 50  0001 C CNN
	3    7900 3450
	1    0    0    -1  
$EndComp
Text Notes 7350 2650 2    28   ~ 0
N or S magnet —> OUTPUT LOW\nNO MAGNET   —> OUPUT HIGH
$Comp
L Device:R R1
U 1 1 5ECE9F8D
P 7150 1800
F 0 "R1" H 7220 1846 50  0000 L CNN
F 1 "270K" H 7220 1755 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 7080 1800 50  0001 C CNN
F 3 "~" H 7150 1800 50  0001 C CNN
F 4 "C22965" H 7150 1800 50  0001 C CNN "LCSC PART #"
	1    7150 1800
	1    0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 5ECEA548
P 7150 2100
F 0 "R2" H 7220 2146 50  0000 L CNN
F 1 "30K" H 7220 2055 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 7080 2100 50  0001 C CNN
F 3 "~" H 7150 2100 50  0001 C CNN
F 4 "C22984" H 7150 2100 50  0001 C CNN "LCSC PART #"
	1    7150 2100
	1    0    0    -1  
$EndComp
$Comp
L Device:R R5
U 1 1 5ECF6566
P 8250 2500
F 0 "R5" V 8150 2500 50  0000 C CNN
F 1 "1.5K" V 8250 2500 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 8180 2500 50  0001 C CNN
F 3 "~" H 8250 2500 50  0001 C CNN
F 4 "C22843" H 8250 2500 50  0001 C CNN "LCSC PART #"
	1    8250 2500
	0    1    1    0   
$EndComp
Wire Wire Line
	6450 2600 6500 2600
$Comp
L Device:R R4
U 1 1 5ECFBED6
P 6100 2250
F 0 "R4" V 5893 2250 50  0000 C CNN
F 1 "100K" V 5984 2250 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 6030 2250 50  0001 C CNN
F 3 "~" H 6100 2250 50  0001 C CNN
F 4 "C25803" H 6100 2250 50  0001 C CNN "LCSC PART #"
	1    6100 2250
	0    1    1    0   
$EndComp
Wire Wire Line
	5650 2600 5650 2250
Wire Wire Line
	5650 2250 5950 2250
Connection ~ 5650 2600
Wire Wire Line
	5650 2600 5750 2600
Wire Wire Line
	6250 2250 6500 2250
Wire Wire Line
	6500 2250 6500 2600
Connection ~ 6500 2600
$Comp
L Device:C C4
U 1 1 5ECFD566
P 6500 2750
F 0 "C4" H 6615 2796 50  0000 L CNN
F 1 "100pF" H 6615 2705 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 6538 2600 50  0001 C CNN
F 3 "~" H 6500 2750 50  0001 C CNN
F 4 "C14858" H 6500 2750 50  0001 C CNN "LCSC PART #"
	1    6500 2750
	1    0    0    -1  
$EndComp
Wire Wire Line
	6500 2900 6500 2950
Wire Wire Line
	6500 2950 6100 2950
Wire Wire Line
	7500 1950 7150 1950
Connection ~ 7150 1950
$Comp
L power:GND #PWR03
U 1 1 5ED0F1E8
P 7150 2250
F 0 "#PWR03" H 7150 2000 50  0001 C CNN
F 1 "GND" H 7155 2077 50  0000 C CNN
F 2 "" H 7150 2250 50  0001 C CNN
F 3 "" H 7150 2250 50  0001 C CNN
	1    7150 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	7500 1950 7500 2400
Wire Wire Line
	6500 2600 7500 2600
$Comp
L Device:LED D2
U 1 1 5ED189F2
P 8700 2850
F 0 "D2" V 8739 2733 50  0000 R CNN
F 1 "LED" V 8648 2733 50  0000 R CNN
F 2 "TerminalBlock:TerminalBlock_bornier-2_P5.08mm" H 8700 2850 50  0001 C CNN
F 3 "~" H 8700 2850 50  0001 C CNN
	1    8700 2850
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR07
U 1 1 5ED19964
P 8700 3000
F 0 "#PWR07" H 8700 2750 50  0001 C CNN
F 1 "GND" H 8705 2827 50  0000 C CNN
F 2 "" H 8700 3000 50  0001 C CNN
F 3 "" H 8700 3000 50  0001 C CNN
	1    8700 3000
	1    0    0    -1  
$EndComp
$Comp
L power:+12V #PWR02
U 1 1 5ED19DF8
P 8700 2050
F 0 "#PWR02" H 8700 1900 50  0001 C CNN
F 1 "+12V" H 8715 2223 50  0000 C CNN
F 2 "" H 8700 2050 50  0001 C CNN
F 3 "" H 8700 2050 50  0001 C CNN
	1    8700 2050
	1    0    0    -1  
$EndComp
$Comp
L Device:R R3
U 1 1 5ED1C800
P 8550 2200
F 0 "R3" V 8450 2200 50  0000 C CNN
F 1 "100K" V 8550 2200 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 8480 2200 50  0001 C CNN
F 3 "~" H 8550 2200 50  0001 C CNN
F 4 "C25803" H 8550 2200 50  0001 C CNN "LCSC PART #"
	1    8550 2200
	0    1    1    0   
$EndComp
Wire Wire Line
	8400 2200 8400 2500
Wire Wire Line
	8700 2050 8700 2200
Connection ~ 8700 2200
Wire Wire Line
	8700 2200 8700 2300
$Comp
L Transistor_FET:ZXMP4A16G Q1
U 1 1 5ED1EA41
P 8600 2500
F 0 "Q1" H 8804 2454 50  0000 L CNN
F 1 "NCE55P15K" H 8804 2545 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:TO-252-2" H 8800 2425 50  0001 L CIN
F 3 "https://datasheet.lcsc.com/szlcsc/Wuxi-NCE-Power-Semiconductor-NCE55P15K_C130102.pdf" H 8600 2500 50  0001 L CNN
	1    8600 2500
	1    0    0    1   
$EndComp
Connection ~ 8400 2500
Text Notes 8000 2600 0    28   ~ 0
R > 12V/10= 1.2KOhm
$Comp
L power:+12V #PWR01
U 1 1 5ED22D3E
P 7150 1650
F 0 "#PWR01" H 7150 1500 50  0001 C CNN
F 1 "+12V" H 7165 1823 50  0000 C CNN
F 2 "" H 7150 1650 50  0001 C CNN
F 3 "" H 7150 1650 50  0001 C CNN
	1    7150 1650
	1    0    0    -1  
$EndComp
$Comp
L Device:C C6
U 1 1 5ED23338
P 7450 3450
F 0 "C6" H 7565 3496 50  0000 L CNN
F 1 "10nF" H 7500 3350 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 7488 3300 50  0001 C CNN
F 3 "~" H 7450 3450 50  0001 C CNN
F 4 "C57112" H 7450 3450 50  0001 C CNN "LCSC PART #"
	1    7450 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	7450 3300 7450 3150
Wire Wire Line
	7450 3150 7800 3150
Connection ~ 7800 3150
Wire Wire Line
	7450 3600 7450 3750
Wire Wire Line
	7450 3750 7800 3750
Connection ~ 7800 3750
$Comp
L Device:C C5
U 1 1 5ED25E5B
P 7100 3450
F 0 "C5" H 7215 3496 50  0000 L CNN
F 1 "100nF" H 7150 3350 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 7138 3300 50  0001 C CNN
F 3 "~" H 7100 3450 50  0001 C CNN
F 4 "C14663" H 7100 3450 50  0001 C CNN "LCSC PART #"
	1    7100 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	7100 3300 7100 3150
Wire Wire Line
	7100 3150 7450 3150
Connection ~ 7450 3150
Wire Wire Line
	7100 3600 7100 3750
Wire Wire Line
	7100 3750 7450 3750
Connection ~ 7450 3750
Text Notes 7500 1750 0    50   ~ 0
I_bias = 400nA\n12/(R1+R2) > 100*I_bias\nR1+R2 <  300K\nR1 = 9R2
$Comp
L Amplifier_Operational:LM358 U2
U 2 1 5ED2BD61
P 8550 3650
F 0 "U2" H 8550 4017 50  0000 C CNN
F 1 "LM358" H 8550 3926 50  0000 C CNN
F 2 "Package_SO:SOP-8_3.9x4.9mm_P1.27mm" H 8550 3650 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm2904-n.pdf" H 8550 3650 50  0001 C CNN
	2    8550 3650
	1    0    0    -1  
$EndComp
Text Label 1900 2600 0    50   ~ 0
12IN
Text Label 5050 2600 0    50   ~ 0
3V3
Text Label 6500 2450 0    50   ~ 0
HALLOUT
Text Label 8400 2350 0    28   ~ 0
GATE
Text Label 8700 2700 0    28   ~ 0
DRAIN
$EndSCHEMATC
