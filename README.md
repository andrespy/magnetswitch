# magnetSwitch
This board controls a 12V non-inductive load by means of a hall effect sensor 
that activates the circuit when no magnet is present. Ideal for automatic lighting 
in boat lockers.
![Front view](magnetSwitchBoard/images/magnetSwitchBoard_front.jpg)
![Back view](magnetSwitchBoard/images/magnetSwitchBoard_back.jpg)

 